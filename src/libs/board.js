
export const WIDTH = window.innerWidth;
export const HEIGHT = window.innerHeight;
export const BOARD_ELEMENTS = 19;

export let boardPos = [
    { x: 0, y: 0 },
    { x: 0, y: -120 },
    { x: 105, y: -60 },
    { x: 105, y: 60 },
    { x: 0, y: 120 },
    { x: -105, y: 60 },
    { x: -105, y: -60 },
    { x: 0, y: -120 - 120 },
    { x: 105, y: -120 - 60 },
    { x: 105 + 105, y: -60 - 60 },
    { x: 105 + 105, y: 0 },
    { x: 105 + 105, y: 60 + 60 },
    { x: 105, y: +120 + 60 },
    { x: 0, y: +120 + 120 },
    { x: -105, y: +120 + 60 },
    { x: -105 - 105, y: 60 + 60 },
    { x: -105 - 105, y: 0 },
    { x: -105 - 105, y: -60 - 60 },
    { x: -105, y: -120 - 60 },
  ];

export function getOffsetFromIndex(index) {
  if (index > 18) {
    return {
      x: 0,
      y: 0,
    };
  }
  return boardPos[index];
}

export function getPosition(centerWidth, centerHeight, scale, positionIndex) {
  return {
    x: centerWidth + getOffsetFromIndex(positionIndex).x * scale,
    y: centerHeight + getOffsetFromIndex(positionIndex).y * scale,
  };
}

export function calculateAngles(angleValueName, anglelIndexes, list) {
  let sumValues = 0;
  anglelIndexes.forEach((indexes) => {
    let internalSum = 0;
    let internalCount = 0;
    let lastvalue = null;
    indexes.forEach((index) => {
      if (!list[index].piece) {
        return;
      }
      let piece = list[index].piece;
      if (lastvalue != null && lastvalue != piece[angleValueName]) {
        return;
      }
      lastvalue = piece[angleValueName];
      internalSum += lastvalue;
      internalCount++;
    });
    if (internalCount != indexes.length) {
      return;
    }
    sumValues += internalSum;
  });
  return sumValues;
}

export function calculateAngles90(list) {
  let anglelIndexes = [
    [15, 16, 17],
    [18, 6, 5, 14],
    [7, 1, 0, 4, 13],
    [8, 2, 3, 12],
    [9, 10, 11],
  ];
  return calculateAngles("value90", anglelIndexes, list);
}

export function calculateAngles45(list) {
  let anglelIndexes = [
    [17, 18, 7],
    [16, 6, 1, 8],
    [15, 5, 0, 2, 9],
    [14, 4, 3, 10],
    [13, 12, 11],
  ];
  return calculateAngles("value45", anglelIndexes, list);
}

export function calculateAngles135(list) {
  let anglelIndexes = [
    [7, 8, 9],
    [18, 1, 2, 10],
    [17, 6, 0, 3, 11],
    [16, 5, 4, 12],
    [15, 14, 13],
  ];
  return calculateAngles("value135", anglelIndexes, list);
}
