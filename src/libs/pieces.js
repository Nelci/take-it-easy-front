
let picesColors = {
    '1':'Azure',
    '2':'pink',
    '3':'LightCoral',
    '4':'LightSkyBlue',
    '5':'darkseagreen',
    '6':'brown',
    '7':'LightGreen',
    '8':'orange',
    '9':'yellow'
}

function buildPiece(angle90,angle45,angle135) {
    return {
        id: angle45 + "-" + angle90 + "-" + angle135,
        value90: angle90,
        color90: picesColors[angle90],
        value45: angle45,
        color45: picesColors[angle45],
        value135: angle135,
        color135: picesColors[angle135],
    }
}

export function newPieces() {
    let pieces = [];
    [1,5,9].forEach(angle90 => {
        [2,6,7].forEach(angle45 => {
            [3,4,8].forEach(angle135 => {
                pieces.push(buildPiece(angle90,angle45,angle135))
            });
        });
    }); 
    return pieces
}

export function newRandomPieces() {
    return newPieces().sort(function(){return 0.5 - Math.random()});
}